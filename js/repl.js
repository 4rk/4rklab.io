const host = window.location.hostname;
const path = window.location.pathname;
const args = path.split("/").filter(Boolean);
const feed = "/feed.json";

var db;
var session;

var stdin;
var stdout;

var wrapper = document.getElementById("wrapper");
var content = document.getElementById("content");
var clone = document.getElementById("clone");

var cli = document.getElementById("cli");
var input = document.getElementById("input");
var msg = document.getElementById("msg");

var vrs = document.getElementById("version");
var version = new Date(vrs.innerHTML);


function print () {

	var c = msg.innerHTML + stdin || "";
	var o = stdout || "";

	var newLine = clone.cloneNode(true);
	newLine.getElementsByTagName("SPAN")[0].innerHTML = c;
	newLine.getElementsByTagName("DIV")[0].innerHTML = o;
	newLine.removeAttribute("id");
	
	content.appendChild(newLine);
	
	session.push({cli:c,out:o});
	sessionStorage.setItem("4rk.2.session", JSON.stringify(session));

	wrapper.scrollTop = wrapper.scrollHeight;
}


function parse () {

	console.log(stdin);
	console.log(stdout);

	print();
}



function populate () {

	for (var line of session.reverse()) {

		var newLine = clone.cloneNode(true);
		newLine.getElementsByTagName("SPAN")[0].innerHTML = line.cli;
		newLine.getElementsByTagName("DIV")[0].innerHTML = line.out;
		newLine.removeAttribute("id");
		
		content.insertBefore(newLine, content.firstChild);
	}

	wrapper.scrollTop = wrapper.scrollHeight;
}


function repl () {

	populate();

	input.value = "";
	input.addEventListener("keydown", function(e){

		if (e.which == 13 || e.keyCode == 13) {

			e.preventDefault();
			e.stopPropagation();

			stdin = input.value;
			input.value = "";

			parse();
		}
	});
}


function reload (callback) {

	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {

			db = JSON.parse(this.responseText);
			db.stamp = version;
			localStorage.setItem("4rk.2.json", JSON.stringify(db));

			callback();
		}
	};

	xmlhttp.open("GET", "https://" + host + feed, true);
	xmlhttp.send();
}


function main () {
	
	if (typeof(Storage) !== "undefined") {
		
		session = JSON.parse(sessionStorage.getItem("4rk.2.session"));
		
		if (! session) {
			
			session = [];
			sessionStorage.setItem("4rk.2.session", JSON.stringify(session));
		}

		db = JSON.parse(localStorage.getItem("4rk.2.json"));

		if (db) {

			db.stamp = new Date(db.stamp);

			if (db.stamp.getTime() === version.getTime()) {

				repl();

			} else {

				reload(repl);
			}

		} else {
			
			reload(repl);
		}
		
	} else {
		
		console.log("No Web Storage support...");
	}
}

main();


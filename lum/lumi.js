const svgns = "http://www.w3.org/2000/svg";

function ease (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 };

class Viz {

	constructor () {
		
		this._x = undefined;
		this._y = undefined;
	}

	get x () {
		return this._x;
	}
	
	set x (x) {
		this._x = x;
	}

	get y () {
		return this._y;
	}
	
	set y (x) {
		this._y = x;
	}
}


class nViz extends Viz {

	constructor () {
		
		super();
		
		this._a = undefined; 	// angle
		this._v = undefined;	// velocity
	}

	get a () { 
		return this._a;
	}
	
	set a (x) {
		this._a = x;
	}

	get v () {
		return this._v;
	}
	
	set v (x) {
		this._v = x;
	}
}	


class tViz extends Viz {

	constructor () {
		
		super();
		
		this._a = undefined;	// angle
		this._c = undefined;	// croissance
	}

	get a () { 
		return this._a;
	}
	
	set a (x) {
		this._a = x;
	}

	get c () {
		return this._c;
	}
	
	set c (x) {
		this._c = x;
	}
}



/******************************************************************************
	Neurons
******************************************************************************/

class Dendrite extends tViz {

	constructor (p) {
		
		super();
		this._root = p;
		this._children = [];
	}

	get root () {
		return this._root;
	}
	
	set root (x) {
		this._root = x;
	}
	
	get children () {
		return this._children;
	}
	
	set children (x) {
		this._children = x;
	}
}


class Axone extends tViz {
	
	constructor (p) {
	
		super();

		this._root = p;
		this._children = [];
		
		this.a = Math.random() * Math.PI * 2;
		this.c = Math.random() * 2;
	
		this.x = p.x;
		this.y = p.y;

	}

	get root () {
		return this._root;
	}
	
	set root (x) {
		this._root = x;
	}
	
	get children () {
		return this._children;
	}
	
	set children (x) {
		this._children = x;
	}
	
}


class Neuron extends nViz {
	
	constructor ({type = "default"} = {}) {
		
		super();
		
		this._dendrites = undefined;
		this._dSize = 0;
		this._axones = undefined;
		this._aSize = 0;

		this.x = (Math.random() * 2 - 1) * 100;
		this.y = (Math.random() * 2 - 1) * 100;
	
		this.v = Math.random();
		this.a = Math.random() * Math.PI * 2;
	}


	get dendrites () {
		return this._dendrites;
	}
	
	set dendrites (x) {
		this._dendrites = x;
	}

	get dSize () {
		return this._dSize;
	}
	
	set dSize (x) {
		this._dSize = x;
	}

	get axones () {
		return this._axones;
	}
	
	set axones (x) {
		this._axones = x;
	}

	get aSize () {
		return this._aSize;
	}
	
	set aSize (x) {
		this._aSize = x;
	}


	randomGrowth () {

		if (! (Math.floor(Math.random() * Math.min(this.aSize, 2) * 500))) {
			
			if (this.axones == undefined) {
		
				var a = new Axone(this);
				a.x = a.root.x + Math.cos(a.a) * a.c;
				a.y = a.root.y + Math.sin(a.a) * a.c;

				this.axones = a;
				this.aSize += 1;
				
			} else {
			
				var stk = [this.axones];
				var nbr = Math.floor(Math.random() * this.aSize) + 1;
		
				while (nbr > 0) {
			
					nbr -= 1;
					var a = stk.shift();

					if (a.children.length > 0) {
						stk = [...stk, ...a.children];
					}

					if (nbr == 0) {
					
						var na = new Axone(a);
						na.x = na.root.x + Math.cos(na.a) * na.c;
						na.y = na.root.y + Math.sin(na.a) * na.c;

						a.children.push(na);
						this.aSize += 1;
					}
				}
			}
		}
	}
	

	neuraLoop (x, f) {

		if (! (x == undefined)) {
				var stk = [x];
				
				while (stk.length > 0) {
					
					var a = stk.shift();

					if (a.children.length > 0) {
						stk = [...stk, ...a.children];
					}

					f(a);
				}
		}
	}
	

	dendritesLoop (f) {
		this.neuraLoop(this.dendrites, f);
	}


	axonesLoop (f) {
		this.neuraLoop(this.axones, f);
	}


	axonesPhysics (a) {
	
		if (! Math.floor(Math.random() * 500)) {
			a.a = (a.a * (Math.random() * 100 - 50)) % (Math.PI * 2);
		}

		if (! Math.floor(Math.random() * 200)) {
			a.c = Math.min(5, a.c * Math.random() * 5);
		}

		a.x = a.root.x + Math.cos(a.a) * a.c;
		a.y = a.root.y + Math.sin(a.a) * a.c;
	}


	axonesMove () {
		
		this.axonesLoop (this.axonesPhysics);
	}


}


/******************************************************************************
	DB
******************************************************************************/

class DB {

	constructor () {
		
		this._NNs = [];		// NeuroNes
		this._NTs = []; 	// NeuroTransmitters
		this._NaNs = []; 	// Not a Neuron
	}

	get NNs () {
		return this._NNs;
	}

	set NNs (x) {
		this._NNs = x;
	}

	get NTs () {
		return this._NTs;
	}

	set NTs (x) {
		this._NTs = x;
	}

	get NaNs () {
		return this._NaNs;
	}

	set NaNs (x) {
		this._NaNs = x;
	}


	add ({neurons = []} = {}) {
		
		if (neurons.length > 0) {
			
			this.NNs = [...this.NNs, ...neurons];
		}
	}

	dump () {
		
		console.log("\n\n -------- Dump -------- ")
			
		console.log("NNs _________");
		console.log(this.NNs);

		console.log("NTs _________");
		console.log(this.NTs);

		console.log("NaNs _________");
		console.log(this.NaNs);

		console.log(" ---------------------- \n\n\n")
	}
}




/******************************************************************************
	Drawer
******************************************************************************/

class Drawer {

	constructor () {
	
		this._canvas = document.createElement('canvas');
		this._ctx = this._canvas.getContext('2d');

		this._canvas.id = "Layer";
		this._canvas.width = window.innerWidth;
		this._canvas.height = window.innerHeight;
		this._canvas.style.position = "fixed";
		this._canvas.style.left = "0";
		this._canvas.style.top = "0";

		this._ctx.translate(this._canvas.width * 0.5, this._canvas.height * 0.5);

		var body = document.getElementsByTagName("body")[0];
		body.appendChild(this._canvas);
		
		this._db = undefined;
		this._timer = undefined;
	}


	get canvas () {
		return this._canvas;
	}

	set canvas (x) {
		this._canvas = x;
	}

	get ctx () {
		return this._ctx;
	}

	set ctx (x) {
		this._ctx = x;
	}


	get timer () {
		return this._timer;
	}


	set timer (x) {
		this._timer = x;
	}


	get db () {
		return this._db;
	}


	set db (x) {
		this._db = x;
	}


	drawNeuron (n) {

		var mt = 3;

		this.ctx.beginPath();
		this.ctx.arc(n.x*mt, n.y*mt, mt, 0, 2 * Math.PI, false);
		this.ctx.fillStyle = 'red';
		this.ctx.fill();
}


	drawAxonesAux (a) {

		if (a.children.length > 0) {
			for (var c of a.children) {
				this.drawAxonesAux(c);
			}
		}

		var mt = 3;

		this.ctx.beginPath();
		this.ctx.moveTo(a.root.x*mt, a.root.y*mt);
		this.ctx.lineTo(a.x*mt, a.y*mt);
		this.ctx.stroke();

		this.ctx.beginPath();
		this.ctx.arc(a.x*mt, a.y*mt, mt, 0, 2 * Math.PI, false);
		this.ctx.fillStyle = 'green';
		this.ctx.fill();


	}


	drawAxones (n) {
		
		if (n.aSize > 0) {
			
				this.drawAxonesAux (n.axones);
		}
	}


	draw () {

		console.log("Drawing");
	
		var w = this.canvas.width;
		var h = this.canvas.height;

		this.ctx.clearRect(w / -2, h / -2, w, h);

		for (var n of this.db.NNs) {
			
			this.drawAxones(n);	
			this.drawNeuron(n);
		}
	}


	start () {

		var self = this;	

		this.timer = setInterval(function(){
			self.draw();
		}, 1);	
	}

	stop () {
		clearInterval(this.timer);
	}

	connect (x) {
		this.db = x;
	}

}


/******************************************************************************
	Clock
******************************************************************************/

class Clock {

	constructor () {

		this._db = undefined;
		this._timer = undefined;
	}
	
	get timer () {
		return this._timer;
	}
	
	set timer (x) {
		this._timer = x;
	}

	get db () {
		return this._db;
	}
	
	set db (x) {
		this._db = x;
	}

	work () {

		console.log("Working");

		for (var n of this.db.NNs) {
				
			//n.randomGrowth();

			if (! Math.floor(Math.random() * 10)) {
				
				n.v = n.v + (ease(Math.random()) * 2 - 1);
				n.v = Math.min(1, Math.max(0, n.v));

				n.x = n.x + n.a * n.v;
				n.y = n.y + n.a * n.v;
			}

			//n.axonesMove();

			
		}
	}

	start () {

		var self = this;		

		this.timer = setInterval(function(){
			self.work();
		}, 1);
	}

	stop () {
		clearInterval(this.timer);
	}
	
	connect (x) {
		this.db = x;
	}
	
}


/******************************************************************************
	Lumi
******************************************************************************/

class Lumi {

	constructor () {
		
		this._drawer = new Drawer();
		this._clock = new Clock();
		this._db = new DB();

		this._drawer.connect(this._db);
		this._clock.connect(this._db);	
	}

	get drawer () {
		return this._drawer;
	}

	set drawer (x) {
		this._drawer = x;
	}

	get clock () {
		return this._clock;
	}

	set clock (x) {
		this._clock = x;
	}

	get db () {
		return this._db;
	}

	set db (x) {
		this._db = x;
	}


	init ({ neurons = [] } = {}) {
		
		console.log("Initializing...");
		
		if (neurons.length > 0) {
			
			console.log(neurons.length + " neurons found");

		} else {
			
			console.log("No neurons");
			
			this.create();
		}
	}
	
	create ({size = 10, type="default"} = {}) {

		console.log("Creating...");
		
		var stk = [];

		for (var k = 0; k < size; k++) {
			
			stk.push(new Neuron(type));
		}
			
		this._db.add({neurons: stk});
	}

	start () {

		console.log("Starting...");

		this.drawer.start();
		this.clock.start();
	}

	stop () {

		console.log("Stopping...");
		
		this.drawer.stop();
		this.clock.stop();
	}

	log () {
		
		this.db.dump();
	}
}

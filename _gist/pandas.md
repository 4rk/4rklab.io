---
title: Pandas
description: Pandas CheatSheet
tags: Python
---

## Basics

`import pandas as pd`



## Series

`series = pd.Series([1,"yo",np.nan, 4])`
```
0    1.0
1    "yo"
2    NaN
3    4.0
dtype: object
```



## DataFrame

`df = pd.DataFrame(np.array([1,2,3,4,5,6]).reshape(2,3))`
```
   0  1  2
0  1  2  3
1  4  5  6
```


`df = pd.DataFrame(np.array([1,2,3,4,5,6]).reshape(2,3), columns=list('ABC'), index=list('XY'))`
```
   A  B  C
X  1  2  3
Y  4  5  6
```


`df.dtypes`
```
0    int32
1    int32
2    int32
dtype: object
```

## Utilities

`df.head()`

`df.iloc[:5]`


`df.tail()`

`df.iloc[-5:]`


`df.describe()`


`df.sort_values(by='A', ascending=False)`


## Naming

```
df2 = df.rename(columns=lambda c: chr(65+c))

df2.loc[:5, 'A':'D']

     A      B      C      D
0   1.0   1.12   1.24   1.36
1   4.0   4.12   4.24   4.36
2   7.0   7.12   7.24   7.36
3  10.0  10.12  10.24  10.36
4  13.0  13.12  13.24  13.36
5  16.0  16.12  16.24  16.36


df2.loc[:5, ('A','D')]

     A      D
0   1.0   1.36
1   4.0   4.36
2   7.0   7.36
3  10.0  10.36
4  13.0  13.36
5  16.0  16.36
```


## Read

`df = pd.read_csv('file.csv')`




---
title: conversion 
description: yolo
tags: js
---

## JSON to CSV

```javascript

function JSONtoCSV (items) {
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(items[0]);
    let csv = items.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    csv = csv.join('\r\n');

    return csv;
}
```


## CSV to JSON

```javascript

/* in : csv contents with headers */

function CSVToJSON(csv){

  var lines=csv.split('\n');

  var result = [];

  var headers=lines[0].split(',');
  lines.splice(0, 1);
  lines.forEach(function(line) {
    var obj = {};
    var currentline = line.split(',');
    headers.forEach(function(header, i) {
      obj[header] = currentline[i];
    });
    result.push(obj);
  });

  return result; //JavaScript object
  //return JSON.stringify(result); //JSON
}
```



## CSV to Array

```javascript

/* in : csv string, default delimiter */

function CSVToArray( strData, strDelimiter ){

    strDelimiter = (strDelimiter || ",");

    // regular expression to parse the CSV values
    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );

    // array to hold our data
    var data = [];
    var headers = [];
    var headersFound = false;
    var headerIndex = 0;

    // array to hold our individual matching groups
    var matches = null;


    // looping over the regex matches
    while (matches = objPattern.exec(strData)){

        var delimiter = matches[1];

        if (delimiter.length && delimiter !== strDelimiter){

            // add an empty row to our data array.
            data.push( {} );
            headersFound = true;
            headerIndex = 0;
        }

        var value;

        if (matches[2]){

            // quoted value, unescape any double quotes
            value = matches[2].replace(new RegExp( "\"\"", "g" ),"\"");

        } else {

            // non-quoted value.
            value = matches[3];

        }

        if (!headersFound) {
          headers.push(value);
        } else {
          data[data.length-1][headers[headerIndex]] = value;
          headerIndex ++;
        }
    }

    // Return the parsed data.
    return(data);
}


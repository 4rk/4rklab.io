---
title: javascript
description: yolo
tags: js
---

```javascript

function yo (t) {
    console.log(t||"yo");
}


function yolo (t) {
    console.log(t) {
}

```



## String

#### Find

```javascript
let sample = "Hello yolo";

sample.includes("yolo");
// true

sample.includes("foo");
// false
```



## Array

#### Create

```javascript
var sparseArray = new Array(3);
// [ , , ]
```

```javascript
var denseArray = Array.apply(null, Array(3));
// [undefined, undefined, undefined]
```


#### Copy

Shallow copy

```javascript
const originalArray = [1, 2, 3];
const shallowArrayClone = [...originalArray];
```


Remove duplicate

```javascript
const duplicateValues = [1, 2, 3, 3, 1, 5];
const unique = [...new Set(duplicateValues)];
```


Flatten multi dimensional arrays

```javascript
const toFlatten = [[1,2,3], [4,5,6], [7,8,9]];
const flattened = [].concat(...toFlatten);
```


#### Find

```javascript
let arr = ["one", "two"];

arr.includes("two");
// true

arr.includes("foo");
// false
```

#### Flatening

```javascript

const animals = [['🐕'], ['😺', '🐈', ['😿'], '😻']];


const levelOneFlat = animals.flat();
// const levelOneFlat = animals.flat(1);
// ['🐕', '😺', '🐈', ['😿'], '😻'];


const infinyFlat = animals.flat(Infinity);
// ['🐕', '😺', '🐈', '😿', '😻'];


const map = animals.flatMap(x => [x]);
//

```


## Objects

#### Copy 

Shallow copy

```javascript
const originalObject = { a:1, b: 2, c: 3 };
const shallowObjectClone = {...originalObject};
```


#### Loop

```javascript
const obj = { a:1, b:2, c:3 };
for (let prop of Object.entries(obj)) {
    console.log(prop);
}
// ['a', 1] ['b', 2] ['c', 3]
```

#### Destructuring 

```javascript
const obj = { x: 1 };

// Grabs obj.x as { x }
const { x } = obj;

// Grabs obj.x as as { otherName }
const { x: otherName } = obj;
```



## Switch

#### Methods Lookup

```javascript
function doSomething (condition) {

  var stuff = {

    'one': function () {
      return 'one';
    },

    'two': function () {
      return 'two';
    }
  };

  if (typeof stuff[condition] !== 'function') {
    return 'default';
  }


  return stuff[condition]();
}
```



## Load file

#### XMLHttpRequest

```javascript
function loadFile(url, callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', url, true);
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            callback(xobj.responseText);
          }
    };
    xobj.send(null);
 }
 ```

#### await

```javascript
await (
    await fetch('https://api.gitlab.com/users/4rk')
).json();
```



## Misc

Grabs the highest timer id & clears everything less than that

```javascript
var highestTimeoutId = setTimeout(";");
for (var i = 0 ; i < highestTimeoutId ; i++) {
    clearTimeout(i); 
}
```

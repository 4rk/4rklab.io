---
title: vim
description: yolo
tags: vim
---

## General

 - `.` : execute last modification
 - `%` : jump to a matching opening or closing parenthesis, square bracket or a curly brace
 - `qa` : place next commands in registry a
 - `q` : end recording
 - `@a` : replay recording


## Moving

 - `:goto 21490` : go to the 21490th byte in the buffer


## Commands

 - `:sh[ell]` : open a new shell
 - `:! cmd` : run cmd
 - `:!!` : run previous cmd
 - `:! % [args]` : run current file 


## Indenting

 - `:%retab!` : retabulate the whole file


---
title: REPL
description: Read–Eval–Print Loop
tags: js node
---

Read–Eval–Print Loop

 - `_` Underscore : last return value


## Dots Commands

 - `.load <file>` : load a file into current session

 - `.break` : end current multi-line
 
 - `.editor` : CTRL+D to end, CTRL+C to cancel

 - `.save <file>` : save current session

 - `.exit`



---
title: R lang
description: R lang
tags: R
---

## General

`getwd()` : Get working directory

`setwd(`fileURL`)` : Set working directory


`source(`file`)` : load file into session

`source(`file`, print.eval=TRUE)` : load file into session displaying evaluation

`source(`file`, echo=T)` : load file into session displaying evaluation



`rm(list=ls())` : clean session

`set.seed(777)`

`data = na.omit(data)` : remove missing values



`str(x)`

`sumary(x)`

`class(x)` : return object type




## Basics

`x:y` : range between x and y 


#### Variables

`x = 3`

`x <- 3`


#### Vector (concat)

`vect = c(x,y,z)` : brut values

`vect = c(x:y)` : range


###### Operations

`vect ^ 2` : operation on vector


###### Access

`vect[2]` : direct access

`vect[x:y]` : range access

`vect[-(x:y)]` : inverted range

`vect[x<y]` : restricted range


###### Utilities

`summary(vect)`: summary of the vector

`length(vect)`

`sort(vect)`

`min(vect)`

`max(vect)`

`sum(vect)`

`mean(vect)`

`median(vect)`

`sd(vect)`

`var(vect)`

`pmax(vect)` : max of individues (vect of vects)



## Matrix

#### Creation

`matrix(c(1, 2, 3, 4, 5, 6), nrow = 2, ncol = 3)`

`matrix(1:6, nrow = 2, ncol = 3, byrow = TRUE, dimnames = list(c("X","Y","Z"), c("A","B","C")))`

`colnames(x) <- c("A","B","C")`

`rownames(x) <- c("X","Y","Z")`


`m[c(1,2),c(2,3)]` : select rows 1 & 2 and columns 2 & 3

`m[x,]` : subsetting by row

`m[,x]` : subsetting by columns

`m[c(3,2),]` : leaving column field blank will select entire columns

`m[-1,]` : all rows except first

`m[c(TRUE,FALSE),c(2,3)]` : logical selection

`m[m>5]` : select elements greater than 5

`m[m%%2 == 0]` : select even elements`

`m[1,,drop=FALSE]` : now the result is a matrix rather than a vector`


#### Utilities

`t(m)` : transpose a matrix

`dim(m) <- c(1,6);` : change dimensions


## Normaliser

`dataset.cr = scale(dataset)` : centered and/or scaled columns matrix

`dataset.d = dist(dataset.cr)` : distance matrix


## Kmeans 

`dataset.kmeans = kmeans(dataset.cr, 4, nstart = 5)` : k = 4

`clusplot(dataset.cr, dataset.kmeans$cluster, color=TRUE, shade=TRUE, labels=2, lines=0)`


#### Inertie totale

`wss <- (nrow(dataset.cr) - 1) * sum(apply(dataset.cr, 2, var))`


#### Inertie intra

```
wssf <- function(Ck) {

    r <- sum(Ck)
    return(r)
}

wssf(dataset.kmeans$withinss)
```

#### Coude

```
coude <- function(){

    A = matrix( 0, nrow=2, ncol=15)
    
    for (k in 1:15) {
        kmeans = kmeans(dataset.cr,k,nstart = 5)
        A[1,k] = wssf(kmeans$withinss)
        A[2,k] = wss-A[1,k]
    }
    
    plot(1:15, A[1,], type = "b", xlab = "Number of Clusters", ylab = "Inertie intra")
    plot(1:15, A[2,], type = "b", xlab = "Number of Clusters", ylab = "Inertie expliquée")
}

coude()
```

---

K-Mean

 - Cluster : a vector of integers indicating the cluster to which each point is allocated
 - centers : a matrix of cluster centers
 - totss : total sum of square
 - withinss : vector of within-cluster sum of square, one component per cluster
 - tot.withinss : total sum of squares (ie: sum(withinss))
 - betweenss : the between-cluster sum of squares
 - size : number of points in each cluster


---


`fviz_nbclust(mydata, kmeans, method = "gap_stat")` : best number of clusters

`fviz_cluster(dataset.kmeans, data = dataset, palette = "jco", ggtheme = theme_minimal())` : display clusters



## K-Medoid/PAM clusterign

`pam.res <- pam(dataset, 3)`

`fviz_cluster(pam.res)`


## Hierarchical clustering

`res.hc <- hclust(dist(dataset),  method = "ward.D2")`

`fviz_dend(res.hc, cex = 0.5, k = 4, palette = "jco")`

---

`d = dist(dataset, method="euclidian")`

`fit = hclust(d, method="ward`)

`plot(fit)`

`groups = cutree(fit, k=5)`

`rect.hclust(fit, k=5, border=red)` : update with red borders




## Display 

`par(mfrow=c(3,3))` : 9 graphes, 3 columns, 3 lines


#### Graph

`plot(vect)`

`plot(vect, type="h")` : Draw axis on graph

`stripchart(vect)` : axial representation (cloud)

`stripchart(vect, method="stack", pch=20, col="blue")`


#### Histogram

`hist(vect)`

`hist(vect, col="steelblue4", border="white", main="Yolo", xlab="Yo", ylab = "Oy")`

`hist(vect, breaks=c(18,20,24,27,32,38)` : variable amplitude classes


#### Mustach Box

`boxplot(vect)`

`boxplot(vect, col="pink", horizontal=TRUE, notch=TRUE, xlab="Yolo")`



## Files

Read tabular data into R dataframe
`read.table(file, header = FALSE, sep = "", dec = ".")`

Read "comma separated value" files (".csv")
`read.csv(file, header = TRUE, sep = ",", dec = ".", ...)`

use a comma as decimal point and a semicolon as field separator.
`read.csv2(file, header = TRUE, sep = ";", dec = ",", ...)`

Read TAB delimited files
`read.delim(file, header = TRUE, sep = "\t", dec = ".", ...)`
`read.delim2(file, header = TRUE, sep = "\t", dec = ",", ...)`

#### Reading a local file

To import a local .txt or a .csv file, from local dir
`my_data <- read.delim("mtcars.txt")`
`my_data <- read.csv("mtcars.csv")`

To import arbitrary extension file
`my_data <- read.delim(file.choose())`
`my_data <- read.csv(file.choose())`

Prevent conversion into factor 
`my_data <- read.delim(file.choose(), stringsAsFactor = FALSE)`

Choose separator
`my_data <- read.table(file.choose(), header = TRUE, sep = "|", dec = ".")`


#### Reading a web file

`read.delim`, `read.csv` and `read.table` work online
`my_data <- read.delim("http://www.sthda.com/upload/boxplot_format.txt")`



## Writing File

`write.csv(dataset, file = "my_data.csv")` : uses “.” for the decimal point and a comma (“,”) for the separator
`write.csv2(dataset, file = "my_data.csv")` : uses a comma (“,”) for the decimal point and a semicolon (“;”) for the separator

`write.table(dataset, file = "myfile.txt", sep = "\t", row.names = TRUE, col.names = NA)`


## Packages

`install.packages("FactoMineR", dependencies=TRUE)` : installation

`library(FactoMineR)` : chargement




## List 

 - library(cluster)
 - library(factoextra)


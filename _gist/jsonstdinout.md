---
title: JSON stdin/out
description: Read JSON from stdin and print it on stdout
tags: js node
---

```
#!/usr/bin/env node

var stdin = process.stdin,
    stdout = process.stdout,
    inputChunks = [];

stdin.resume();
stdin.setEncoding('utf8');

stdin.on('data', function (chunk) {
    inputChunks.push(chunk);
});

stdin.on('end', function () {

        var inputJSON = inputChunks.join("");

        var cleanJSON = inputJSON.replace(/\\n/g, "\\n")
                       .replace(/\\'/g, "\\'")
                       .replace(/\\"/g, '\\"')
                       .replace(/\\&/g, "\\&")
                       .replace(/\\r/g, "\\r")
                       .replace(/\\t/g, "\\t")
                       .replace(/\\b/g, "\\b")
                       .replace(/\\f/g, "\\f");
        cleanJSON = cleanJSON.replace(/[\u0000-\u0019]+/g,"");

        var parsedJSON = JSON.parse(cleanJSON);
        var outputJSON = JSON.stringify(parsedJSON);

        stdout.write(outputJSON);
        stdout.write('\n');
});

```
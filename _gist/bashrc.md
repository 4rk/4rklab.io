---
title: bashrc
description: yolo
tags: sh
---

```shell

# Reload .bashrc

source ~/.bashrc

```

## Search pattern through files

```shell
grep -rnw '/path/to/somewhere/' -e 'pattern'
```

This will only search through those files which have .c or .h extensions:

```shell
grep --include=\*.{c,h} -rnw '/path/to/somewhere/' -e "pattern"
```

This will exclude searching all the files ending with .o extension:

```shell
grep --exclude=*.o -rnw '/path/to/somewhere/' -e "pattern"

```

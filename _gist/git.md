---
title: git
description: git
tags: sh git
---

## Branching

 - `git log --oneline --graph --color` : What and Where branches are

 - `git checkout` : Shortcut for checkout
 - `git checkout -b` : Create branch and check out

 - `git branch -l` : List all local branches
 - `git branch -r` : List all remote branches
 - `git branch -a` : Show local and remote branches
 - `git branch -d` : Politely ask Git to delete a local branch
 - `git branch -D` : Sternly ask Git to delete a local branch


## Fetching / Syncing / Merging

 - `git fetch -p` : Fetch and prune
 - `!git pull && git push` : sync
 - `!git fetch -p && git merge origin/master` : Merge remote master into the current branch 


## Commits

 - `git commit` : commit
 - `git commit -a` : Commit all tracked
 - `git commit -a -m` : Commit all tracked with message

 - `git reset --hard` : For when you just want it all to go away

 - `git rm --cached` : Forget about a tracked file


## Utilities

 - `git config --get-regexp ^alias\\.` : List all aliases
 - `git config --global -e` : Open .gitconfig in your default editor

 - `git blame <file>`

 - #### git bisect

Divide tree to find bug

```shell
git bisect start
git bisect good <SHA1>
git bisect bad <SHA1>
# ...
git bisect reset
```

 - #### git grep

   - `-n` : number of line
   - `-i` : ignore case
   - `-c` : count matches
   - `-p` : display context



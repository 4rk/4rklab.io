---
title: node.js
description: yolo
tags: js node
---

## Make executable file

Don't forget to `chmod +x`

```javascript
#!/usr/bin/env node
```


## Extract Args

```javascript
const [ , , ...args ] = process.argv;
console.log(args[0]);
```


## Thread exec 

`ping` example

```javascript
var sys = require('sys');
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { 
    sys.puts(stdout);
}
exec("ping -c 3 localhost", puts);
```


Execute job in another thread

```javascript
const res = await job( () => {

    let i = 0;

    // do things

    return i;
});
```



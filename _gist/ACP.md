---
title: ACP with R
description: Principal Component Analysis with R
tags: R
---

## FactoMineR

```R
install.packages(c("FactoMineR", "factoextra", "ggplot2"))

library("FactoMineR")
library("factoextra")
library("ggplot2")
```

## Data Format

Load Data
`data(decathlon2)`

Setting active individuals and active variables for the principal component analysis:
`decathlon2.active <- decathlon2[1:23, 1:10]`


